class RepairShop {
  final String cnpj;
  final String name;
  final List<String> specialties;
  final dynamic address;

  RepairShop(this.cnpj, this.name, this.specialties, this.address);

  RepairShop.fromJson(Map<String, dynamic> json)
      : cnpj = json['cnpj'],
        name = json['name'],
        specialties = json['specialties'],
        address = json['address'];

  Map<String, dynamic> toJson() => {
        'cnpj': cnpj,
        'name': name,
        'specialties': specialties,
        'address': address
      };
}
