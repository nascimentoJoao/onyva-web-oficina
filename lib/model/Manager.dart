class Manager {
  final int id;
  final String email;
  final String name;
  final String username;
  final String password;
  final String phone;
  final String cpf;

  Manager(this.id, this.name, this.cpf, this.email, this.phone, this.username,
      this.password);

  Manager.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        email = json['email'],
        name = json['name'],
        username = json['username'],
        password = json['password'],
        phone = json['phone'],
        cpf = json['cpf'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'email': email,
        'name': name,
        'username': username,
        'password': password,
        'phone': phone,
        'cpf': cpf
      };
}
