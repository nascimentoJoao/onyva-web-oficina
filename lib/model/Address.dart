class Address {
  final String street;
  final String number;
  final String cep;
  final String city;
  final String state;

  Address(this.street, this.number, this.cep, this.city, this.state);

  Address.fromJson(Map<String, dynamic> json)
      : street = json['street'],
        number = json['number'],
        cep = json['cep'],
        city = json['city'],
        state = json['state'];

  Map<String, dynamic> toJson() => {
        'street': street,
        'number': number,
        'cep': cep,
        'city': city,
        'state': state
      };
}
