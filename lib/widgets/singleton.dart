import 'package:onyva_web_app/model/Manager.dart';

class Singleton {
  static Singleton _instance;

  factory Singleton({Manager manager}) {
    _instance ??= Singleton._internalConstructor(manager);
    return _instance;
  }
  Singleton._internalConstructor(this.manager);

  Manager manager;
}
