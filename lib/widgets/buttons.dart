import 'package:flutter/material.dart';
import 'package:vibration/vibration.dart';

Widget buttonBolNavigation(String title, route, BuildContext context) {
  return GestureDetector(
      child: Container(
        //rmargin: const EdgeInsets.only(left: 130.0),
        width: 150.0,
        height: 55.0,
        decoration: BoxDecoration(
            color: Colors.indigo, borderRadius: BorderRadius.circular(15.0)),
        child: Center(
            child: Text(
          title,
          style: TextStyle(color: Colors.white, fontSize: 16),
        )),
      ),
      onTap: () {
        Vibration.vibrate(duration: 70, amplitude: 50);

        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => route));
      });
}

textButton(title, rote, contex) {
  return GestureDetector(
    child: Text(
      "Cadastrar",
      style: TextStyle(color: Colors.indigo),
    ),
    onTap: () {
      Navigator.of(contex).push(MaterialPageRoute(builder: (context) => rote));
    },
  );
}

Widget buttonBolGeneralButton(String title, Function onPressed) {
  return RaisedButton(
      child: Container(
        //rmargin: const EdgeInsets.only(left: 130.0),
        width: 150.0,
        height: 55.0,
        decoration: BoxDecoration(
            color: Color(0xff326771),
            borderRadius: BorderRadius.circular(15.0)),
        child: Center(
            child: Text(
          title,
          style: TextStyle(color: Colors.white, fontSize: 16),
        )),
      ),
      onPressed: onPressed);
}
