//import 'dart:ffi';

import 'package:flutter/material.dart';

class RegularCard extends StatelessWidget {
  final Function onPress;
  final String label;
  final IconData icon;
  final double iconSize;
  static const PrimaryColor = const Color(0xFF326771);

  const RegularCard({
    @required this.label,
    @required this.icon,
    @required this.iconSize,
    this.onPress,
  });

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: 400,
      height: 200,
      decoration: myBoxDecoration(),
      child: RaisedButton.icon(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          onPressed: this.onPress,
          icon: Icon(
            this.icon,
            size: this.iconSize,
            color: Colors.white,
          ),
          label: Text(
            this.label,
            style: TextStyle(color: Colors.white, fontSize: 35),
          ),
          color: PrimaryColor),
    );
  }

  BoxDecoration myBoxDecoration() {
    return BoxDecoration(
      border: Border.all(width: 3.0),
      borderRadius: BorderRadius.all(Radius.circular(15.0) // border radius here
          ),
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.3),
          spreadRadius: 5,
          blurRadius: 7,
          offset: Offset(0, 3), // changes position of shadow
        ),
      ],
    );
  }
}
