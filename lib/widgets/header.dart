import 'package:flutter/material.dart';

class Header extends StatelessWidget implements PreferredSizeWidget {
  final String pageTitle;
  final bool hasGoBack;
  const Header({this.pageTitle = "Bem vindo!", this.hasGoBack = true});
  static const PrimaryColor = const Color(0xFF326771);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: PrimaryColor,
      leading: this.hasGoBack
          ? IconButton(
              icon: Icon(Icons.arrow_back),
              iconSize: 35,
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          : Container(),
      title: Center(child: Text(this.pageTitle)),
    );
  }

  @override
  Size get preferredSize {
    final AppBar appBar = AppBar();
    return new Size.fromHeight(appBar.preferredSize.height);
  }
}
