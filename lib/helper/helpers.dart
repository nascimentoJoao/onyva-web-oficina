class Helpers {
  static String returnStringBasedOnPosition(int position) {
    switch (position) {
      case 0:
        return "Chapeação";
        break;
      case 1:
        return "Manutenção";
        break;
      case 2:
        return "Troca de óleo";
        break;
      case 3:
        return "Troca de pneus";
        break;
      case 4:
        return "Pintura";
        break;
    }
    return "";
  }

  static List<String> constructArrayStrings(List<bool> list) {
    List<String> arraySpecialties = [];

    for (var i = 0; i < list.length; i++) {
      if (list[i]) {
        arraySpecialties.add(returnStringBasedOnPosition(i));
      }
    }

    return arraySpecialties;
  }
}
