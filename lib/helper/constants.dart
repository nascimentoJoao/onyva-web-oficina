class Constants {
  static const String ERROR_MESSAGE =
      "Houve um erro de comunicação externo. Sua solicitação não foi efetuada.\nTente novamente mais tarde.";
  static const String SUCCESS_CREATING_REPAIR_SHOP =
      "O cadastro da sua oficina está em análise.\nVocê receberá um email informando a situação da análise o quanto antes.\nAgradecemos sua compreensão.\n";
  static const String INFO_MESSAGE_FOR_REPAIR_SHOP =
      "Agora você deve preencher os dados da sua oficina para validar seu cadastro.";
}
