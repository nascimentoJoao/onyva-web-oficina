import 'dart:convert';

import 'package:onyva_web_app/model/RepairShop.dart';
import 'package:onyva_web_app/model/Manager.dart';

import 'package:http/http.dart' as http;

//TODO: CONSERTAR O TRATAMENTO DE ERROS CASO NÃO ALCANCE A API

class HttpRequests {
  static Future registerRepairShop(
      Manager manager, RepairShop repairShop) async {
    final http.Response response = await http.post(
      'http://localhost:3000/api/managers',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode({'manager': manager, 'repairShop': repairShop}),
    );

    return response.statusCode;
  }
}
