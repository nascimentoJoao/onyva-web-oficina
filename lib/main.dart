import 'package:flutter/material.dart';
import 'package:onyva_web_app/ui/loginScreen.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() => runApp((MaterialApp(
      initialRoute: '/',
      routes: {
        '/signup': (context) => Login(),
      },
      title: 'Sistema ONYVA',
      home: Login(),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: [const Locale('pt', 'BR')],
    )));
