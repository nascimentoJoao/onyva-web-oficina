import 'package:flutter/material.dart';
import 'package:onyva_web_app/widgets/textFields.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final TextEditingController _usernameController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();
  final _formKey = GlobalKey<FormState>();

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Erro"),
      content: Text("Funcionalidade ainda não disponível."),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  nameButton(int typeButton) {
    if (typeButton == 0) return "Cadastro";
    if (typeButton == 1) return "Acesso";
  }

  styleTextButton(int typeButton) {
    return TextStyle(color: Colors.white, fontSize: 16);
  }

  styleButton(int typeButton) {
    return BoxDecoration(color: Color(0xff326771));
  }

  buttonBooleanNavigation(int typeButton) {
    return RaisedButton(
        child: Container(
          //rmargin: const EdgeInsets.only(left: 130.0),
          width: 100.0,
          height: 55.0,
          decoration: styleButton(typeButton),
          child: Center(
              child: Text(
            nameButton(typeButton),
            style: styleTextButton(typeButton),
          )),
        ),
        onPressed: () {
          if (typeButton == 0) {
            Navigator.of(context).pushNamed('/signup');
          } else if (typeButton == 1) {
            if (_passwordController.text == "" ||
                _usernameController.text == null ||
                _usernameController.text == "") {
              showAlertDialog(context);
            } else {
              Navigator.of(context).pushNamed('/');
            }
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        margin: EdgeInsets.only(top: 40),
        alignment: Alignment.topCenter,
        child: ListView(
          children: <Widget>[
            new Image.asset(
              'onyva-logo.png',
              width: 300.0,
              height: 300.0,
            ),
            Center(
                child: Column(
              children: <Widget>[
                Form(
                  key: _formKey,
                  child: Padding(
                      padding: const EdgeInsets.only(
                          top: 5.0, left: 300.0, right: 300.0),
                      child: Column(
                        children: <Widget>[
                          genericField("Usuário", _usernameController,
                              Icons.person, false),
                          Padding(padding: const EdgeInsets.only(top: 10.0)),
                          genericField("Senha", _passwordController,
                              Icons.vpn_key, true),
                        ],
                      )),
                ),
                Padding(
                    padding: const EdgeInsets.only(
                        top: 70.0, left: 750.0, right: 250.0),
                    child: Row(
                      children: <Widget>[
                        buttonBooleanNavigation(1),
                        Padding(padding: const EdgeInsets.only(left: 100.0)),
                        buttonBooleanNavigation(0)
                      ],
                    ))
              ],
            ))
          ],
        ),
      ),
    );
  }
}
