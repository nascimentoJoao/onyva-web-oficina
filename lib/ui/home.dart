import 'package:flutter/material.dart';
import 'package:onyva_web_app/ui/repairShopForm.dart';
import 'package:onyva_web_app/widgets/input_field.dart';
import 'package:onyva_web_app/ui/loginScreen.dart';
import 'package:onyva_web_app/widgets/singleton.dart';
import 'package:onyva_web_app/model/Manager.dart';
import 'package:onyva_web_app/widgets/input_field_password.dart';

class Home extends StatelessWidget {
  final TextEditingController _nameController = new TextEditingController();
  final TextEditingController _cpfController = new TextEditingController();
  final TextEditingController _emailController = new TextEditingController();
  final TextEditingController _usernameController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();
  final TextEditingController _passwordConfirmation =
      new TextEditingController();
  final TextEditingController _telephoneController =
      new TextEditingController();

  registerSingleton(Manager manager) {
    Singleton(manager: manager);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[50],
      body: Padding(
        padding:
            EdgeInsets.only(top: 60.0, bottom: 60.0, left: 120.0, right: 120.0),
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
          elevation: 5.0,
          child: Container(
            child: Row(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width / 3.3,
                  height: MediaQuery.of(context).size.height,
                  color: Color(0xff326771),
                  child: Padding(
                    padding:
                        EdgeInsets.only(top: 85.0, right: 50.0, left: 50.0),
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 60.0,
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                            child: Text(
                              "Sistema ONYVA",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 30.0,
                                fontWeight: FontWeight.w900,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                            child: Text(
                              "Preencha os dados para efetuar seu cadastro. Agora você deverá preencher seus dados de pessoa física.",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          SizedBox(
                            height: 50.0,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                      top: 15.0, right: 70.0, left: 70.0, bottom: 10.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        "Dados de pessoa física",
                        style: TextStyle(
                            color: Color(0xff326771),
                            fontWeight: FontWeight.w600,
                            fontSize: 35.0,
                            fontFamily: 'Merriweather'),
                      ),
                      const SizedBox(height: 21.0),

                      InputField(
                          label: "Nome",
                          content: "Jose das dores",
                          controller: _nameController),
                      //InputField Widget from the widgets folder

                      SizedBox(height: 20.0),

                      InputField(
                          label: "CPF",
                          content: "000.000.000-00",
                          controller: _cpfController),
                      //InputField Widget from the widgets folder

                      SizedBox(height: 20.0),

                      //InputField Widget from the widgets folder
                      InputField(
                        label: "Email",
                        content: "johndoe@mail.com",
                        controller: _emailController,
                      ),

                      SizedBox(height: 20.0),

                      InputField(
                        label: "Usuário",
                        content: "usuario_onyva",
                        controller: _usernameController,
                      ),

                      SizedBox(height: 20.0),
                      InputFieldPassword(
                        label: "Senha",
                        content: "********",
                        controller: _passwordController,
                      ),

                      SizedBox(height: 20.0),

                      InputFieldPassword(
                        label: "Confirme sua senha",
                        content: "********",
                        controller: _passwordConfirmation,
                      ),
                      SizedBox(height: 20.0),

                      InputField(
                        label: "Telefone:",
                        content: "(00) 00000 000000",
                        controller: _telephoneController,
                      ),

                      //Membership Widget from the widgets folder
                      // Aggrenment(),

                      SizedBox(
                        height: 20.0,
                      ),

                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: 170.0,
                          ),
                          FlatButton(
                            color: Colors.grey[200],
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => Login()));
                            },
                            child: Text("Tela de Login"),
                          ),
                          SizedBox(
                            width: 20.0,
                          ),
                          FlatButton(
                            color: Color(0xff326771),
                            onPressed: () {
                              Manager manager = Manager(
                                  0,
                                  _nameController.text,
                                  _cpfController.text,
                                  _emailController.text,
                                  _telephoneController.text,
                                  _usernameController.text,
                                  _passwordController.text);

                              registerSingleton(manager);

                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => RepairShopForm()));
                            },
                            child: Text(
                              "Avançar",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
