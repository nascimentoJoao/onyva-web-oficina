import 'home.dart';
import 'package:flutter/material.dart';

import 'package:onyva_web_app/ui/loginScreen.dart';

import 'package:onyva_web_app/widgets/input_field.dart';
import 'package:onyva_web_app/widgets/singleton.dart';

import 'package:onyva_web_app/model/RepairShop.dart';
import 'package:onyva_web_app/model/Manager.dart';
import 'package:onyva_web_app/model/Address.dart';

import 'package:onyva_web_app/requests/http_requests.dart';
import 'package:onyva_web_app/helper/helpers.dart';
import 'package:onyva_web_app/helper/constants.dart';

class RepairShopForm extends StatefulWidget {
  @override
  _RepairShopForm createState() => _RepairShopForm();
}

class _RepairShopForm extends State<RepairShopForm> {
  final TextEditingController _cnpjController = new TextEditingController();
  final TextEditingController _repairshopNameController =
      new TextEditingController();
  final TextEditingController _streetNameController =
      new TextEditingController();
  final TextEditingController _streetStateController =
      new TextEditingController();
  final TextEditingController _streetNumberController =
      new TextEditingController();
  final TextEditingController _streetCityController =
      new TextEditingController();
  final TextEditingController _streetCepController =
      new TextEditingController();

  var chapeacaoVal = false;
  var manutencaoVal = false;
  var trocaOleoVal = false;
  var trocaPneusVal = false;
  var pinturaVal = false;

  Widget checkbox(String title, bool boolValue) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(title),
        Checkbox(
          value: boolValue,
          checkColor: Colors.lightGreenAccent[300],
          activeColor: Colors.lightGreenAccent[300],
          onChanged: (bool value) {
            /// manage the state of each value
            setState(() {
              switch (title) {
                case "Chapeação":
                  chapeacaoVal = value;
                  break;
                case "Manutenção":
                  manutencaoVal = value;
                  break;
                case "Troca de óleo":
                  trocaOleoVal = value;
                  break;
                case "Troca de pneus":
                  trocaPneusVal = value;
                  break;
                case "Pintura":
                  pinturaVal = value;
                  break;
              }
            });
          },
        )
      ],
    );
  }

  sendDataToAPI() async {
    List<String> arrayStrings = Helpers.constructArrayStrings(
        [chapeacaoVal, manutencaoVal, trocaOleoVal, trocaPneusVal, pinturaVal]);

    Manager dataFromManager = Singleton().manager;

    Address address = Address(
        _streetNameController.text,
        _streetNumberController.text,
        _streetCepController.text,
        _streetCityController.text,
        _streetStateController.text);

    RepairShop repairShop = RepairShop(_cnpjController.text,
        _repairshopNameController.text, arrayStrings, {'address': address});

    HttpRequests.registerRepairShop(dataFromManager, repairShop);
  }

  showAlertDialog(BuildContext context, String messageForAlertDialog) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => Login()));
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Aviso!"),
      content: Text(messageForAlertDialog),
      actions: [
        okButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[50],
      body: Padding(
        padding:
            EdgeInsets.only(top: 60.0, bottom: 60.0, left: 120.0, right: 120.0),
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
          elevation: 5.0,
          child: Container(
            child: Row(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width / 3.3,
                  height: MediaQuery.of(context).size.height,
                  color: Color(0xff326771),
                  child: Padding(
                    padding:
                        EdgeInsets.only(top: 85.0, right: 50.0, left: 50.0),
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 60.0,
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                            child: Text(
                              "Sistema ONYVA",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 30.0,
                                fontWeight: FontWeight.w900,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                            child: Text(
                              Constants.INFO_MESSAGE_FOR_REPAIR_SHOP,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          SizedBox(
                            height: 50.0,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                      top: 15.0, right: 70.0, left: 70.0, bottom: 10.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        "Dados da Oficina",
                        style: TextStyle(
                            color: Color(0xff326771),
                            fontWeight: FontWeight.w600,
                            fontSize: 35.0,
                            fontFamily: 'Merriweather'),
                      ),
                      const SizedBox(height: 21.0),

                      //InputField Widget from the widgets folder
                      InputField(
                          label: "Nome da oficina",
                          content: "Oficina dos Jedis",
                          controller: _repairshopNameController),

                      SizedBox(height: 20.0),

                      InputField(
                        label: "CNPJ",
                        content: "00.000.000/0000-00",
                        controller: _cnpjController,
                      ),

                      SizedBox(height: 20.0),

                      //InputField Widget from the widgets folder
                      InputField(
                          label: "Rua",
                          content: "Rua dos Alfeneiros",
                          controller: _streetNameController),

                      SizedBox(height: 20.0),

                      InputField(
                          label: "Número",
                          content: "4439",
                          controller: _streetNumberController),

                      SizedBox(height: 20.0),

                      InputField(
                          label: "Cidade",
                          content: "Porto Alegre",
                          controller: _streetCityController),

                      SizedBox(height: 20.0),

                      InputField(
                          label: "Estado",
                          content: "RS",
                          controller: _streetStateController),

                      SizedBox(height: 20.0),

                      InputField(
                          label: "CEP",
                          content: "91780-401",
                          controller: _streetCepController),

                      //Membership Widget from the widgets folder
                      // Aggrenment(),
                      SizedBox(height: 20.0),
                      Row(
                        children: <Widget>[
                          Padding(padding: const EdgeInsets.only(right: 20.0)),
                          Text("Serviços oferecidos: *",
                              style:
                                  TextStyle(fontSize: 14, color: Colors.black)),
                          checkbox("Chapeação", chapeacaoVal),
                          Padding(padding: const EdgeInsets.only(right: 10.0)),
                          checkbox("Manutenção", manutencaoVal),
                          Padding(padding: const EdgeInsets.only(right: 10.0)),
                          checkbox("Troca de óleo", trocaOleoVal),
                          Padding(padding: const EdgeInsets.only(right: 10.0)),
                          checkbox("Troca de pneus", trocaPneusVal),
                          Padding(padding: const EdgeInsets.only(right: 10.0)),
                          checkbox("Pintura", pinturaVal),
                        ],
                      ),

                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: 170.0,
                          ),
                          FlatButton(
                            color: Colors.grey[200],
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => Home()));
                            },
                            child: Text("Voltar"),
                          ),
                          SizedBox(
                            width: 20.0,
                          ),
                          FlatButton(
                            color: Color(0xff326771),
                            onPressed: () {
                              sendDataToAPI();
                            },
                            child: Text(
                              "Salvar",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
