import 'package:flutter/material.dart';
import 'package:onyva_web_app/widgets/header.dart';
import 'package:onyva_web_app/widgets/regular_card.dart';
import 'package:onyva_web_app/ui/calendar.dart';

class HomePage extends StatelessWidget {
  static const routeName = '/';

  const HomePage({Key key}) : super(key: key);

  void goTo(String routeNameTo, BuildContext ctx) {
    Navigator.of(ctx).pushNamed(routeNameTo);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Header(
        hasGoBack: false,
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            RegularCard(
                label: "Notificações",
                icon: Icons.notifications_none_sharp,
                iconSize: 42,
                onPress: () => {}),
            RegularCard(
                label: "Agenda",
                icon: Icons.calendar_today,
                iconSize: 35,
                onPress: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => Calendar()));
                }),
            RegularCard(
              label: "Histórico",
              icon: Icons.history,
              iconSize: 40,
              onPress: () {},
            ),
          ],
        ),
      ),
    );
  }
}
