import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:onyva_web_app/model/RepairShop.dart';
import 'package:onyva_web_app/widgets/buttons.dart';
import 'package:onyva_web_app/ui/login.dart';
import 'package:onyva_web_app/model/Manager.dart';
import 'package:onyva_web_app/widgets/textFields.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:http/http.dart' as http;

class Signup extends StatefulWidget {
  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  final TextEditingController _nameController = new TextEditingController();
  final TextEditingController _cpfController = new TextEditingController();
  final TextEditingController _emailController = new TextEditingController();
  final TextEditingController _usernameController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();
  final TextEditingController _passwordConfirmation =
      new TextEditingController();
  final TextEditingController _telephoneController =
      new TextEditingController();
  final TextEditingController _cnpjController = new TextEditingController();
  final TextEditingController _addressController = new TextEditingController();
  final TextEditingController _repairshopNameController =
      new TextEditingController();

  var cpfMaskFormatter = new MaskTextInputFormatter(
      mask: '###.###.###-##', filter: {"#": RegExp(r'[0-9]')});
  var telephoneMaskFormatter = new MaskTextInputFormatter(
      mask: '(##) #####-####', filter: {"#": RegExp(r'[0-9]')});
  var cnpjMaskValidator = new MaskTextInputFormatter(
      mask: '##.###.###/####-##', filter: {"#": RegExp(r'[0-9]')});

  var chapeacaoVal = false;
  var manutencaoVal = false;
  var trocaOleoVal = false;
  var trocaPneusVal = false;
  var pinturaVal = false;

  Widget checkbox(String title, bool boolValue) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(title),
        Checkbox(
          value: boolValue,
          onChanged: (bool value) {
            /// manage the state of each value
            setState(() {
              switch (title) {
                case "Chapeação":
                  chapeacaoVal = value;
                  break;
                case "Manutenção":
                  manutencaoVal = value;
                  break;
                case "Troca de óleo":
                  trocaOleoVal = value;
                  break;
                case "Troca de pneus":
                  trocaPneusVal = value;
                  break;
                case "Pintura":
                  pinturaVal = value;
                  break;
              }
            });
          },
        )
      ],
    );
  }

  String returnStringBasedOnPosition(int position) {
    switch (position) {
      case 0:
        return "Chapeação";
        break;
      case 1:
        return "Manutenção";
        break;
      case 2:
        return "Troca de óleo";
        break;
      case 3:
        return "Troca de pneus";
        break;
      case 4:
        return "Pintura";
        break;
    }
    return "";
  }

  List<String> constructArrayStrings(List<bool> list) {
    List<String> arraySpecialties = [];

    for (var i = 0; i < list.length; i++) {
      if (list[i]) {
        arraySpecialties.add(returnStringBasedOnPosition(i));
      }
    }

    return arraySpecialties;
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => Login()));
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Aviso!"),
      content: Text(
          "O cadastro da sua oficina está em análise.\nEnviaremos um e-mail para: ${_emailController.text}\ninformando a situação da análise o quanto antes.\nAgradecemos sua compreensão.\n"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget checkAndNavi() {
      bool isOk = true;

      if (isOk) {
        return buttonBolGeneralButton("Finalizar", () async {
          Manager manager = Manager(
              0,
              _nameController.text,
              _cpfController.text,
              _emailController.text,
              _telephoneController.text,
              _usernameController.text,
              _passwordConfirmation.text);

          var arrayStrings = constructArrayStrings([
            chapeacaoVal,
            manutencaoVal,
            trocaOleoVal,
            trocaPneusVal,
            pinturaVal
          ]);

          //print(arrayStrings);

          RepairShop repairShop = RepairShop(
              _cnpjController.text,
              _repairshopNameController.text,
              arrayStrings,
              {'address': _addressController.text});

          final http.Response response = await http.post(
            'http://localhost:3000/api/managers',
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: jsonEncode({'manager': manager, 'repairShop': repairShop}),
          );

          // print(manager.toJson());
          // print(repairShop.toJson());

          print(response.body);

          showAlertDialog(context);
        });
      }
    }

    return Scaffold(
        backgroundColor: Colors.white,
        body: ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 100.0, top: 30, right: 100.0),
              child: Column(
                children: <Widget>[
                  Text("Dados de pessoa física"),
                  Text("Nome completo: *",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  genericField(
                      "Jose da Silva", _nameController, Icons.person, false),
                  Padding(padding: const EdgeInsets.only(top: 10.0)),
                  Text("CPF: *",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  genericFieldWithMask("000.000.000-00", _cpfController,
                      Icons.assignment_ind, false, cpfMaskFormatter),
                  Padding(padding: const EdgeInsets.only(top: 10.0)),
                  Text("E-mail: *",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  genericField("joao.silva@mail.com", _emailController,
                      Icons.mail, false),
                  Padding(padding: const EdgeInsets.only(top: 10.0)),
                  Text("Usuário: *",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  genericField(
                      "joaoSilva123", _usernameController, Icons.person, false),
                  Padding(padding: const EdgeInsets.only(top: 10.0)),
                  Text("Senha: *",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  genericField(
                      "********", _passwordController, Icons.vpn_key, true),
                  Padding(padding: const EdgeInsets.only(top: 10.0)),
                  Text("Confirme sua senha: *",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  genericField(
                      "********", _passwordConfirmation, Icons.vpn_key, true),
                  Padding(padding: const EdgeInsets.only(top: 10.0)),
                  Text("Telefone: *",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  genericFieldNumber("(00) 0000-00000", _telephoneController,
                      Icons.phone, false, telephoneMaskFormatter),
                  Padding(padding: const EdgeInsets.only(top: 10.0)),
                  Text("Nome da oficina: *",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  genericField("Oficina do Jedi", _repairshopNameController,
                      Icons.location_on, false),
                  Padding(padding: const EdgeInsets.only(top: 10.0)),
                  Text("Endereço da oficina: *",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  genericField("Rua dos Alfeneiros, 392, Porto Alegre",
                      _addressController, Icons.location_on, false),
                  Padding(padding: const EdgeInsets.only(top: 10.0)),
                  Text("CNPJ: *",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  genericFieldNumber("00.000.000/0000-00", _cnpjController,
                      Icons.location_city, false, cnpjMaskValidator),
                  Padding(padding: const EdgeInsets.only(top: 10.0)),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 100.0, top: 30, right: 100.0),
              child: Row(
                children: <Widget>[
                  Text("Serviços oferecidos: *",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  checkbox("Chapeação", chapeacaoVal),
                  Padding(padding: const EdgeInsets.only(right: 10.0)),
                  checkbox("Manutenção", manutencaoVal),
                  Padding(padding: const EdgeInsets.only(right: 10.0)),
                  checkbox("Troca de óleo", trocaOleoVal),
                  Padding(padding: const EdgeInsets.only(right: 10.0)),
                  checkbox("Troca de pneus", trocaPneusVal),
                  Padding(padding: const EdgeInsets.only(right: 10.0)),
                  checkbox("Pintura", pinturaVal),
                ],
              ),
            ),
            Center(
                child: Padding(
                    padding: EdgeInsets.only(top: 70, left: 0, bottom: 8),
                    child: checkAndNavi()))
          ],
        ));
  }
}
